const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const validator = require('express-validator');
const session = require('express-session');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const passport = require('passport');
const helpers = require('app/helpers');
const rememberLogin = require('app/http/middleware/RememberLogin');
module.exports = class Application {
    constructor() {
        this.setupExpress();
        this.setConfig();
        this.setMongoConection();
        this.setRouters();
    }
    setupExpress() {
        const server = http.createServer(app);
        server.listen(config.port, () => console.log(`Listening on port ${config.port}`));
    }
    setConfig() {
        require('app/passport/passport-local');
        app.use(express.static(config.layout.public_dir));
        app.set('view engine', config.layout.view_engin);
        app.set('views', config.layout.view_dir);
        app.use(config.layout.ejs.expressLayout);
        app.set("layout extractScripts",config.layout.ejs.extractScripts);
        app.set("layout extractStyles", config.layout.ejs.extractStyles);
        app.set('layout',config.layout.ejs.master);
        app.use(bodyParser.json());
        app.use(validator())
        app.use(session({...config.session }));
        app.use(cookieParser(config.cookie_secrctkey));
        app.use(flash());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(rememberLogin.handel);
        app.use((req, res, next) => {
            app.locals = new helpers(req, res).getObjects();
            next();
        })
    }
    setMongoConection() {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.database.url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    }
    setRouters() {
        app.use(require('app/routes/web'))
        app.use(require('app/routes/api'))
    }
}