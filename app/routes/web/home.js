const express = require('express');
const router = express.Router();

//controllers
const Homecontrollers = require('app/http/controllers/Homecontrollers');
//Home Routes
router.get('/', Homecontrollers.index);

router.get('/logout', (req, res) => {
    res.clearCookie('remember_token');
    req.logout();
    res.redirect('/');
})
module.exports = router;