const express = require('express');
const router = express.Router();

//admin controllers
const admincontrollers = require('app/http/controllers/admin/Admincontrollers');
const courseController = require('app/http/controllers/admin/courseController');

router.use((req , res , next)=>{
    res.locals.layout = "admin/master";

    next();
})

//admin Routes
router.get('/',admincontrollers.index);
router.get('/courses',courseController.index)
router.get('/courses/create',courseController.create);

module.exports = router;