const express = require('express');
const router = express.Router();

//middleware
const redirectifAuthenticated = require('app/http/middleware/redirectifAuthenticated');
const redirectifNotAdmin = require('app/http/middleware/redirectifNotAdmin');
//admin Router
const adminRouter = require('app/routes/web/admin');
router.use('/admin',redirectifNotAdmin.handel,adminRouter);

//Home Router
const homeRouter = require('app/routes/web/home');
router.use('/',homeRouter);

//auth Router
const authRouter = require('app/routes/web/auth');
router.use('/auth',redirectifAuthenticated.handel,authRouter);

module.exports = router;