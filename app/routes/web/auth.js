const express = require('express');
const router = express.Router();
//Controller
const loginController = require('app/http/controllers/auth/loginController');
const regiserController = require('app/http/controllers/auth/registerController');

//validators
const registerValidator = require('app/http/validator/Registervalidator');
const loginValidator = require('app/http/validator/loginvalidator');

//Routes
router.get('/login',loginController.showLoginForm);
router.post('/login',loginValidator.handel(), loginController.loginProccess);
router.get('/register',regiserController.showRegisterationForm);
router.post('/register',registerValidator.handel(), regiserController.registerProccess);

module.exports = router;