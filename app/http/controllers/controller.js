const autoBind = require('auto-bind');
const { validationResult } = require('express-validator/check');
module.exports = class controller {
    constructor() {
        autoBind(this);
    }
    async validationData(req) {
        const result = validationResult(req);
        if (!result.isEmpty()) {
            const errors = result.array();
            const messages = [];
            errors.forEach(err => { messages.push(err.msg) })
            if (messages.length == 0)
                return true
            req.flash('errors', messages);
            return false;
        }
        return true;

    }
}