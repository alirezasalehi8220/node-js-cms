const controller = require('app/http/controllers/controller');

class courseController extends controller{
    index(req , res){
        const title = 'دوره ها' ;
        res.render('admin/courses/index',{title});
    }
    create(req , res){
        res.render('admin/courses/create');
    }
}
module.exports = new courseController();