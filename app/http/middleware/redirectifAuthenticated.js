const User = require('app/models/user');
const middleware = require('app/http/middleware/middleware');
class redirectifAuthenticated extends middleware {
    handel(req, res, next) {
        if (req.isAuthenticated()) {
            res.redirect('/');
        }else{
            next();
        }

    }

}
module.exports = new redirectifAuthenticated();