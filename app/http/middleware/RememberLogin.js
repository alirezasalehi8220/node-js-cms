const User = require('app/models/user');
const middleware = require('app/http/middleware/middleware');
class RememberLogin extends middleware {
    handel(req, res, next) {
        if (!req.isAuthenticated()) {
            const rememberToken = req.signedCookies.remember_token;
            if (rememberToken) {
                return this.userFind(req, rememberToken, next);
            } else next();
        } else {
            next();
        }

    }
    userFind(req, rememberToken, next) {
        User.findOne({ 'rememberToken': rememberToken }, (err, user) => {
            if (err) return next(err);
            if (user) {
                req.logIn(user, err => {
                    if (err) {
                        console.log('error');
                        next(err);
                    } else next();
                });
            } else {
                next();
            }
        })
    }
}
module.exports = new RememberLogin();