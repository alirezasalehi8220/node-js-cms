const middleware = require('./middleware');

class redirectifNotAdmin extends middleware{
        handel(req , res , next){
            if(req.isAuthenticated() && req.user.admin){
                return next();
            }
            return res.redirect('/');
        }
}

module.exports = new redirectifNotAdmin();