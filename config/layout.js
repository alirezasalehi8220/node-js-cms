const path = require('path');
const expressLayout = require('express-ejs-layouts');
module.exports = {
    public_dir: 'public',
    view_dir: path.resolve('./resource/views'),
    view_engin: 'ejs',
    ejs :{
        expressLayout:expressLayout,
        extractScripts :true,
        extractStyles :true,
        master : 'home/master'
    }

}