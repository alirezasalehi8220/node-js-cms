const database = require('./database');
const session = require('./session');
const layout = require('./layout');
module.exports = {
    database: database,
    port: process.env.APPLICATION_PORT,
    session,
    layout,
    cookie_secrctkey: process.env.COOKIE_SECRETKEY,
}